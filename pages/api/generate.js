import { Configuration, OpenAIApi } from 'openai';

const configuration = new Configuration({
	apiKey: process.env.OPENAI_API_KEY,
});

const openai = new OpenAIApi(configuration);

const basePromptPrefix = `
A single US healthcare and health insurance fact regarding`;

const generateAction = async (req, res) => {
	const prompt = `${basePromptPrefix}\n`;
	console.log(`API: ${prompt}`);
	//console.log(`API: ${basePromptPrefix}${req.body.userInput}\n`);

	const baseCompletion = await openai.createCompletion({
		model: 'text-davinci-003',
		prompt: `${basePromptPrefix}`,
		temperature: 0.7,
		max_tokens: 150,
	});

	const basePromptOutput = baseCompletion.data.choices.pop();

	const secondPrompt = `
    write a blog post about these facts:
        ${basePromptOutput.text}
    `;

	const secondCompletion = await openai.createCompletion({
		model: 'text-davinci-003',
		prompt: `${secondPrompt}`,
		temperature: 0.7,
		max_tokens: 250,
	});

	const secondPromptOutput = secondCompletion.data.choices.pop();

	res.status(200).json({ output: secondPromptOutput });
};

export default generateAction;
